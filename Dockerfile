FROM tomcat

RUN apt-get update

COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/xb.war
EXPOSE 8080

CMD ["catalina.sh", "run"]